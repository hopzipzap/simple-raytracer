#ifndef RTV1_H
# define RTV1_H

# include <stdbool.h>
# include <stdio.h>
# include <math.h>
# include <fcntl.h>
# include <errno.h>
# include <pthread.h>
# include <sys/types.h>
# include <sys/stat.h>
# include "../utils/libft/libft.h"
# include "../utils/gnl/get_next_line.h"
# include <SDL2/SDL.h>

# define WIDTH 1024
# define HEIGHT 768
# define FOV M_PI / 6
# define THREADS 8
# define DIM 4
# define MOVESTEP 0.01
# define MOVESPEED 2

typedef struct	s_vec3
{
	float		x;
	float		y;
	float		z;
}				t_vec3;

typedef struct	s_sphere
{
	t_vec3		*center;
	float		radius;
}				t_sphere;

typedef struct	s_triangle
{
	t_vec3		a;
	t_vec3		b;
	t_vec3		c;
}				t_triangle;

typedef struct			s_rgb
{
	int				r;
	int				g;
	int				b;
}						t_rgb;

typedef struct			s_image
{
	Uint32			width;
	Uint32			height;
	Uint32			length;
	Uint32			*pixels;
}						t_image;

typedef struct			s_ui_node
{
	t_image			*img;
	SDL_Rect		rect;
}						t_ui_node;

typedef struct			s_controls
{
	bool			w;
	bool			a;
	bool			s;
	bool			d;
	bool			space;
	bool			c;
	bool			rotup;
	bool			rotdown;
	bool			rotleft;
	bool			rotright;
}						t_controls;


/* RASUL */

typedef enum	e_objtype
{
	T_CAMERA = -2,
	T_PLANE = 1,
	T_CUBE = 2,
	T_SPHERE = 3,
	T_CONE = 4,
	T_CYLINDER = 5,
	T_PYRAMID = 6,
	T_TRIANGLE = 7,
	T_POINTLIGHT = 8,
}				t_objtype;

typedef struct	s_shape
{
	float			width;
	float			height;
	float			depth;
	float			radius;
	float			origin;
	float			angle;
}				t_shape;

typedef struct		s_transform
{
	t_vec3		position;
	t_vec3		rotation;
	t_vec3		direction;
}					t_transform;

typedef struct			s_world
{
	t_vec3			up;
	t_vec3			right;
	t_vec3			fwd;
	t_vec3			look_at;
}						t_world;

typedef struct	s_object
{
	t_vec3			center; 
	float			distance;
	t_vec3			hit;
	t_triangle		tr;
	t_vec3			normal;
	t_transform		transform;
	t_world			world;
	t_shape			shape;
	t_objtype		object_type;
	t_vec3			vcolor;
	int				color;
	float			brightness;
	int				material;
}				t_object;

typedef struct	s_app
{
	t_list			*read_list;
	t_object		*objects;
	int				objs_count;
	//t_hitpoint		hit;
}				t_app;

typedef struct	s_window
{
	SDL_Window		*window;
	SDL_Surface		*screen;
	SDL_Event		e;
	Uint32			*ptr;
	bool			power;
	bool			running;
	t_controls		controls;
	t_vec3			*frame_buf;
	t_object		camera;
	t_vec3			dir;
	t_vec3			temp;
	t_object		**lights;
	t_app			*app;
	t_vec3			background;
	int				start_r;
	int				end_r;
	bool			temp_tr;
}				t_window;

/* Initialize */
int		global_init(t_window *w);
void	life_cycle(t_window *w);

/* Vectors */
t_vec3		vec_3(float x, float y, float z);
t_vec3		normalize(t_vec3 vec);
t_vec3		sum_v(t_vec3 vec_a,t_vec3 vec_b);
t_vec3		sub_v(t_vec3 vec_a,t_vec3 vec_b);
float		mults_sv(t_vec3 vec,float scal);
float		dot_pr(t_vec3 vec_a,t_vec3 vec_b);
t_vec3		cross_v(t_vec3 vec_a,t_vec3 vec_b);
t_vec3		multiply_by_scalar(t_vec3 vec, float scalar);
t_vec3		rotate_y(t_vec3 vec, float angle);
t_vec3		rotate_x(t_vec3 vec, float angle);
void		handle_free(t_vec3 **vec_orig, t_vec3 *new);
float		length(t_vec3 vec);

/* Render Image */
void		render(t_window *w, void *f);
void		*region_render(void *w);
void		draw_framebuf(t_window *w, t_vec3 *frame_buf, Uint32 width, Uint32 height, Uint32 start_height);
t_vec3		light_shader(t_window *w, t_object *obj, float dist, t_object **lights);

/* Shapes */
bool	cast_rays(t_window *w, t_object *objs, int objcount, t_object *ret);
bool	sphere_intersect(t_vec3 orig, t_vec3 dir, t_object *obj);
bool	plane_intersect(t_vec3 orig, t_vec3 dir, t_object *obj);
bool	triangle_intersect(t_vec3 orig, t_vec3 dir, t_object *obj);
bool	point_intersect(t_vec3 orig, t_vec3 dir, t_object *obj);

/* Color */
t_vec3		rgb_to_vec3(int red, int green, int blue);
t_rgb		vec3_to_rgb(t_vec3 vec3);
t_vec3		hex_to_vec3(int hex);
Uint32		set_color(t_rgb rgb);
Uint32		form_color(int r, int g, int b);
t_rgb		*inter_color(Uint32 *pixels, float x_diff, float y_diff, int w);

/* Controls */
void	event_cycle(t_window *w);
bool	key_action(SDL_Event *ev, int32_t key_code, Uint32 press);
bool	switch_key(t_window *w, int32_t key, bool *key_tr);
bool	handle_key(t_window *w, int32_t key, bool *key_tr);

/* Reader */
t_list	*read_file_arg(int argc, char **argv);
t_list	*read_file(char *path);

/* Parser */
void	parse_list(t_list *lst, t_window *w);

/* Additional */
int ft_rm_char(char *dest, char c);
t_vec3     get_vector3(char *str);


int		init_framebuf(t_window *w);
void	init_lights(t_window *w);
int		init_orientation(t_object *obj);

t_object    *find_obj_by_type(t_app *app, int value);
#endif