/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 20:48:21 by rfunk             #+#    #+#             */
/*   Updated: 2019/10/17 17:14:28 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/rtv1.h"

int		init_sdl(t_window *w)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return (1);
	w->window = SDL_CreateWindow("Rtv1", SDL_WINDOWPOS_CENTERED,
	SDL_WINDOWPOS_CENTERED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	w->screen = SDL_GetWindowSurface(w->window);
	w->power = true;
	return (0);
}

int		init_camera(t_window *w)
{
	w->camera.transform.position = vec_3(62.946476, 70.493355, 170.502914);
	w->camera.world.look_at = vec_3(-50., -35., 1.);
	w->camera.world.fwd = normalize(sub_v(w->camera.world.look_at, w->camera.transform.position));
	w->camera.world.up = normalize(vec_3(.0, 1., .0));
	w->camera.world.right = normalize(cross_v(w->camera.world.fwd, w->camera.world.up));
	return (0);
}

void	init_lights(t_window *w)
{
	w->lights = (t_object **)malloc(sizeof(t_object *) * 2);
}

int		init_framebuf(t_window *w)
{
	w->frame_buf = (t_vec3 *)malloc(sizeof(t_vec3) * WIDTH * HEIGHT);
	return (0);
}

int		global_init(t_window *w)
{
	init_sdl(w);
	init_framebuf(w);
	init_camera(w);
	w->running = false;
	w->temp_tr = false;

	return (0);
}