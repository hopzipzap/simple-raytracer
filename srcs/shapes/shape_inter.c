/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shape_inter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/23 17:11:45 by rfunk             #+#    #+#             */
/*   Updated: 2019/10/17 17:16:30 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/rtv1.h"

bool	point_intersect(t_vec3 orig, t_vec3 dir, t_object *obj)
{
	obj->shape.radius = 1;
	obj->vcolor = hex_to_vec3(0xffffff); // remove
	if (sphere_intersect(orig, dir, &(*obj)))
		return (true);
	return (false);
}

bool	triangle_intersect(t_vec3 orig, t_vec3 dir, t_object *obj)
{
	t_vec3 edge1 = sub_v(obj->tr.b, obj->tr.a);
	t_vec3 edge2 = sub_v(obj->tr.c, obj->tr.a);

	t_vec3 pvec = cross_v(dir, edge2);

	float det = dot_pr(edge1, pvec);

	if (det < 0.000001 && det > -0.000001)
		return (false);
	float	inv_det = 1. / det;
	
	t_vec3 tvec = sub_v(orig, obj->tr.a);
	
	float	u = dot_pr(tvec, pvec) * inv_det;
	if (u < 0.0 || u > 1.)
		return (false);
	
	t_vec3	qvec = cross_v(tvec, edge1);

	float	v = dot_pr(dir, qvec) * inv_det;
	if (v < 0.0 || v + u > 1.)
		return (false);
	
	float	t = dot_pr(edge2, qvec) * inv_det;

	obj->distance = t;
	return (true);
}

bool	plane_intersect(t_vec3 orig, t_vec3 dir, t_object *obj)
{
	float denom;

	denom = dot_pr(sub_v(obj->tr.a, obj->tr.b), cross_v(dir, sub_v(obj->tr.c, obj->tr.a)));
	if (denom < 1e-8 && denom > -1e-8)
		return (false);
	denom = dot_pr(sub_v(obj->tr.c, obj->tr.a), cross_v(sub_v(orig, obj->tr.a),
			sub_v(obj->tr.a, obj->tr.b))) * (1. / denom);
	obj->distance = denom;
	return (true);
}

bool	sphere_intersect(t_vec3 orig, t_vec3 dir, t_object *obj)
{
	t_vec3	l;
	
	l = sub_v(obj->transform.position, orig);
	float tca = dot_pr(dir, l);
	float d2 = dot_pr(l, l) - tca * tca;
	if (d2 > obj->shape.radius * obj->shape.radius)
		return (false);
	float thc = sqrtf(obj->shape.radius * obj->shape.radius - d2);
	obj->distance = tca - thc;
	float t1 = tca + thc;
	if (obj->distance < 0)
		obj->distance = t1;
	if (obj->distance < 0)
		return (false);
	return (true);
}
