/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   image.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 20:06:32 by rfunk             #+#    #+#             */
/*   Updated: 2019/09/25 18:46:14 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/rtv1.h"

void		draw_framebuf(t_window *w, t_vec3 *frame_buf, Uint32 width, Uint32 height, Uint32 start_height)
{
	Uint32 x;
	Uint32 y;
	Uint32 color;
	x = 0;
	while (x < width)
	{
		y = start_height;
		while (y < height)
		{
			color = set_color(vec3_to_rgb(frame_buf[width * y +x]));
			//printf("%u\n",color);
			w->ptr[width * y + x] = color;
			y++;
		}
		x++;
	}
}
