/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec3.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rfunk <rfunk@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/09/19 20:52:46 by rfunk             #+#    #+#             */
/*   Updated: 2019/10/14 14:34:38 by rfunk            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/rtv1.h"
# define DTR(k)	((double)k * (M_PI / 180))

void	handle_free(t_vec3 **vec_orig, t_vec3 *new)
{
	if (*vec_orig)
		free(*vec_orig);
	*vec_orig = new;
}

t_vec3		vec_3(float x, float y, float z)
{
	t_vec3	vec3;

	vec3.x = x;
	vec3.y = y;
	vec3.z = z;
	return (vec3);
}

t_rgb		vec3_to_rgb(t_vec3 vec3)
{
	t_rgb	rgb;
	
	// know why 170.0f instead 255.0f
	if (vec3.x > 1.)
		rgb.r = 255.0F;
	else
		rgb.r = vec3.x * 255.0F;
	if (vec3.y > 1.)
		rgb.g = 255.0F;
	else
		rgb.g = vec3.y * 255.0F;
	if (vec3.z > 1.)
		rgb.b = 255.0F;
	else
		rgb.b = vec3.z * 255.0F;
	return (rgb);
}

t_vec3		hex_to_vec3(int hex)
{
	return (vec_3(((hex / 256 / 256) % 256) / 255.0f,
	((hex / 256) % 256) / 255.0f, ((hex) % 256) / 255.0f));
}

t_vec3		rgb_to_vec3(int red, int green, int blue)
{
	return (vec_3(((float)red) / 255.F, ((float)green) / 255.F, ((float)blue) / 255.F));
}

t_vec3		sub_v(t_vec3 vec_a,t_vec3 vec_b)
{
	t_vec3	ret;

	ret.x = vec_a.x - vec_b.x;
	ret.y = vec_a.y - vec_b.y;
	ret.z = vec_a.z - vec_b.z;
	return (ret);
}

float		dot_pr(t_vec3 vec_a,t_vec3 vec_b)
{
	return (vec_a.x * vec_b.x + vec_a.y * vec_b.y + vec_a.z * vec_b.z);
}

float		mults_sv(t_vec3 vec,float scal)
{
	return (vec.x * scal + vec.y * scal + vec.z *scal);
}

float		length(t_vec3 vec)
{
	float length;

	length = sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
	return (length);
}

t_vec3		cross_v(t_vec3 vec_a,t_vec3 vec_b)
{
	t_vec3 ret;
 
	ret.x = vec_a.y * vec_b.z - vec_a.z * vec_b.y;
	ret.y = vec_a.z * vec_b.x - vec_a.x * vec_b.z;
	ret.z = vec_a.x * vec_b.y - vec_a.y * vec_b.x;
	return (ret);
}
 
t_vec3		sum_v(t_vec3 vec_a,t_vec3 vec_b)
{
	t_vec3 ret;

	ret.x = vec_a.x + vec_b.x;
	ret.y = vec_a.y + vec_b.y;
	ret.z = vec_a.z + vec_b.z;
	return (ret);
}

// remake func to procedure // wrong
t_vec3		multiply_by_scalar(t_vec3 vec, float scalar)
{
	t_vec3	ret;

	ret.x = vec.x * scalar;
	ret.y = vec.y * scalar;
	ret.z = vec.z * scalar;
	return (ret);
}

// remake func to procedure
t_vec3		normalize(t_vec3 vec)
{
	float	length;

	
	length = sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
	return (multiply_by_scalar(vec, 1 / length));
}


// float		**create_basis_m(t_vec3 *vecs)
// {
// 	int i;
// 	float **matrix;

// ///////////////////
// 	matrix = (float **)malloc(sizeof(float *) * 4);
// 	int j = 0;
// 	while (j < DIM)
// 	{
// 		matrix[j] = (float *)malloc(sizeof(float) * 4); 
// 		j++;
// 	}
// ///////////////////

// 	i = 0;
// 	while (i < DIM)
// 	{
// 		matrix[0][i] = vecs[i].x;
// 		matrix[1][i] = vecs[i].y;
// 		matrix[2][i] = vecs[i].z;
// 		matrix[3][i] = 1;
// 		i++; 
// 	}
// 	return (matrix);
// }

// void		offset_m(float ***matrix, t_vec3 vec)
// {
// 	*matrix[0][DIM - 1] += vec.x;
// 	*matrix[1][DIM - 1] += vec.y;
// 	*matrix[2][DIM - 1] += vec.z;
// 	return ;
// }

// void		rotation_m(float ***matrix, float angle)
// {
// 	float tr[3][3];
	
// 	tr[0][0] = cos(angle);
// 	tr[0][2] = sin(angle);
// 	tr[1][1] = 1;
// 	tr[2][0] = -sin(angle);
// 	tr[2][2] = cos(angle);

// 	*matrix[0][2] = 
// }

t_vec3		rotate_y(t_vec3 vec, float angle)
{
	t_vec3 ret;
	float tr[3][3];
	
	tr[0][0] = cos(angle);
	tr[0][2] = sin(angle);
	tr[1][1] = 1;
	tr[2][0] = -sin(angle);
	tr[2][2] = cos(angle);

	// flag shit
	tr[0][1] = 0;
	tr[1][0] = 0;
	tr[1][2] = 0;
	tr[2][1] = 0;

	ret.x = vec.x * tr[0][0] + vec.y * tr[0][1] + vec.z * tr[0][2];
	ret.y = vec.x * tr[1][0] + vec.y * tr[1][1] + vec.z * tr[1][2];
	ret.z = vec.x * tr[2][0] + vec.y * tr[2][1] + vec.z * tr[2][2];
	return (ret);
}

t_vec3		rotate_x(t_vec3 vec, float angle)
{
	t_vec3 ret;
	float tr[3][3];

	tr[0][0] = 1;
	tr[1][1] = cos(angle);
	tr[1][2] = -sin(angle);
	tr[2][1] = sin(angle);
	tr[2][2] = cos(angle);

	// flag shit
	tr[0][1] = 0;
	tr[0][2] = 0;
	tr[1][0] = 0;
	tr[2][0] = 0;

	ret.x = vec.x * tr[0][0] + vec.y * tr[0][1] + vec.z * tr[0][2];
	ret.y = vec.x * tr[1][0] + vec.y * tr[1][1] + vec.z * tr[1][2];
	ret.z = vec.x * tr[2][0] + vec.y * tr[2][1] + vec.z * tr[2][2];
	return (ret);
}


/* z
	tr[0][0] = cos(angle);
	tr[0][1] = -sin(angle);
	tr[2][0] = sin(angle);
	tr[2][1] = cos(angle);
	tr[2][2] = 1;
	*/

/* y
	tr[0][0] = cos(angle);
	tr[0][2] = sin(angle);
	tr[1][1] = 1;
	tr[2][0] = -sin(angle);
	tr[2][2] = cos(angle);
	*/

/* x
	tr[0][0] = 1;
	tr[1][1] = cos(angle);
	tr[1][2] = -sin(angle);
	tr[2][1] = sin(angle);
	tr[2][2] = cos(angle);
	*/